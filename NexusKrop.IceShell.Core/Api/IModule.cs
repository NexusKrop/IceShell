﻿namespace NexusKrop.IceShell.Core.Api;

using NexusKrop.IceShell.Core.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public interface IModule
{
    void Initialize();
}
