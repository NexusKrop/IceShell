﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NexusKrop.IceShell.Core.Exceptions {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ER {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ER() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("NexusKrop.IceShell.Core.Exceptions.ER", typeof(ER).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Required complex argument values cannot have perceeding optional complex argument values..
        /// </summary>
        internal static string ComplexArgumentOrderFailure {
            get {
                return ResourceManager.GetString("ComplexArgumentOrderFailure", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid option argument.
        /// </summary>
        internal static string ComplexInvalidOption {
            get {
                return ResourceManager.GetString("ComplexInvalidOption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Missing required option &apos;{0}&apos;.
        /// </summary>
        internal static string ComplexMissingRequiredOption {
            get {
                return ResourceManager.GetString("ComplexMissingRequiredOption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No such option &apos;{0}&apos;.
        /// </summary>
        internal static string ComplexNonExistingOption {
            get {
                return ResourceManager.GetString("ComplexNonExistingOption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Option &apos;{0}&apos; excepts no value but found value.
        /// </summary>
        internal static string ComplexNoValueAllowed {
            get {
                return ResourceManager.GetString("ComplexNoValueAllowed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Value arguments cannot be preceeded by option arguments.
        /// </summary>
        internal static string ComplexPreceedingOption {
            get {
                return ResourceManager.GetString("ComplexPreceedingOption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Option &apos;{0}&apos; excepts value but found nothing.
        /// </summary>
        internal static string ComplexValueRequired {
            get {
                return ResourceManager.GetString("ComplexValueRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Directory not found.
        /// </summary>
        internal static string DirBadDirectory {
            get {
                return ResourceManager.GetString("DirBadDirectory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Excepted begin of quote but found nothing.
        /// </summary>
        internal static string ExceptedBeginOfQuote {
            get {
                return ResourceManager.GetString("ExceptedBeginOfQuote", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Excepted end of quote but found nothing.
        /// </summary>
        internal static string ExceptedEndOfQuote {
            get {
                return ResourceManager.GetString("ExceptedEndOfQuote", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Excepted string but found nothing.
        /// </summary>
        internal static string ExceptedString {
            get {
                return ResourceManager.GetString("ExceptedString", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid path..
        /// </summary>
        internal static string InvalidPath {
            get {
                return ResourceManager.GetString("InvalidPath", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid command attribute.
        /// </summary>
        internal static string ManagerInvalidAttribute {
            get {
                return ResourceManager.GetString("ManagerInvalidAttribute", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to More than one CommandAttribute was found..
        /// </summary>
        internal static string ManagerMoreThanOneAttribute {
            get {
                return ResourceManager.GetString("ManagerMoreThanOneAttribute", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type specified does not implement ICommand..
        /// </summary>
        internal static string ManagerTypeNotCommand {
            get {
                return ResourceManager.GetString("ManagerTypeNotCommand", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} values required but only {1} found.
        /// </summary>
        internal static string MissingValues {
            get {
                return ResourceManager.GetString("MissingValues", resourceCulture);
            }
        }
    }
}
