# IceShell

An alternative, cross-platform shell written in C#, and inspired by DOS.

## Installation

### Prebuilt Binaries

Prebuilt binaries are _not available_ in this stage of development. Once they are available, they
can be seen in [the releases page](https://codeberg.org/NexusKrop/IceShell/releases).

### Build from Source

See [BUILDING](BUILDING.md) file.

## Contributing

For code contributions, pull requests are welcome. For major changes, please open an issue first to discuss what
you like to change.

You can work on issues that have have the tag `Status: Open` or `Status: Help wanted`. See [Issues](https://codeberg.org/NexusKrop/IceShell/issues).
You may also report bugs or suggest features through the Issues page.

## License

[GPL-3.0-or-later](COPYING.txt)

